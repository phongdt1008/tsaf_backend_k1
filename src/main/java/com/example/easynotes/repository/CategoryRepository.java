/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.easynotes.repository;

import com.example.easynotes.model.Category;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Phong
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    List<Category> findByTitle(String title);

    @Query(value = "select * from Category c where c.title = ?1", nativeQuery = true)
    List<Category> findByTitleNativeQuery(String title);

    @Query(value = "select c from Category c where c.title = ?1", nativeQuery = false)
    List<Category> findByTitleJPQLQuery(String title);
}
