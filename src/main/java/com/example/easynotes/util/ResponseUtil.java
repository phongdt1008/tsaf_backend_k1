/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.easynotes.util;

import com.example.easynotes.contants.StatusCode;
import com.example.easynotes.model.Category;
import com.example.easynotes.model.Note;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.List;

/**
 *
 * @author Phong
 */
public class ResponseUtil {

    private static ObjectMapper mapper = new ObjectMapper();

    public static String success(JsonNode body) {
        ObjectNode node = mapper.createObjectNode();
        node.put("StatusCode", StatusCode.SUCCESS.getValue());
        node.put("Message", StatusCode.SUCCESS.getValue());
        node.set("Response", body);
        return node.toString();
    }

    public static String notfound() {
        ObjectNode node = mapper.createObjectNode();
        node.put("StatusCode", StatusCode.NOT_FOUND.getValue());
        node.put("Message", StatusCode.NOT_FOUND.getValue());
        node.put("Response", "");
        return node.toString();
    }

    public static String invalid() {
        ObjectNode node = mapper.createObjectNode();
        node.put("StatusCode", StatusCode.PARAMETER_INVALID.getValue());
        node.put("Message", StatusCode.PARAMETER_INVALID.getValue());
        node.put("Response", "");
        return node.toString();
    }

    public static String serverError() {
        ObjectNode node = mapper.createObjectNode();
        node.put("StatusCode", StatusCode.SERVER_ERROR.getValue());
        node.put("Message", StatusCode.SERVER_ERROR.getValue());
        node.put("Response", "");
        return node.toString();
    }

    public static ObjectNode returnNote(Note note) {
        ObjectNode node = mapper.createObjectNode();
        node.put("id", note.getId());
        node.put("title", note.getTitle());
        node.put("content", note.getContent());
        return node;
    }

    public static ArrayNode returnListNode(List<Note> notes) {
        ArrayNode node = mapper.createArrayNode();
        for (Note note : notes) {
            node.add((returnNote(note)));
        }
        return node;
    }

    public static ObjectNode returnCategory(Category category) {
        ObjectNode node = mapper.createObjectNode();
        node.put("id", category.getId());
        node.put("title", category.getTitle());
        node.put("content", category.getContent());
        node.set("notes", returnListNode(category.getNotes()));
        return node;
    }

    public static ArrayNode returnListCategory(List<Category> categorys) {
        ArrayNode node = mapper.createArrayNode();
        for (Category category : categorys) {
            node.add((returnCategory(category)));
        }
        return node;
    }
}
