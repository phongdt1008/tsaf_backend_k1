package com.example.easynotes.controller;

import com.example.easynotes.exception.ResourceNotFoundException;
import com.example.easynotes.model.Tag;
import com.example.easynotes.repository.TagRepository;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class TagController {

    @Autowired
    TagRepository noteRepository;

    @GetMapping("/tags")
    public List<Tag> getAllTags() {
        return noteRepository.findAll();
    }

    @PostMapping("/tags")
    public Tag createTag(@Valid @RequestBody Tag note) {
        return noteRepository.save(note);
    }

    @GetMapping("/tags/{id}")
    public Tag getTagById(@PathVariable(value = "id") Long noteId) {
        return noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFoundException("Tag", "id", noteId));
    }

    @PutMapping("/tags/{id}")
    public Tag updateTag(@PathVariable(value = "id") Long noteId,
            @Valid @RequestBody Tag noteDetails) {

        Tag note = noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFoundException("Tag", "id", noteId));

        note.setTitle(noteDetails.getTitle());
        note.setContent(noteDetails.getContent());

        Tag updatedTag = noteRepository.save(note);
        return updatedTag;
    }

    @DeleteMapping("/tags/{id}")
    public ResponseEntity<?> deleteTag(@PathVariable(value = "id") Long noteId) {
        Tag note = noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFoundException("Tag", "id", noteId));

        noteRepository.delete(note);

        return ResponseEntity.ok().build();
    }
}
