package com.example.easynotes.controller;

import com.example.easynotes.exception.ResourceNotFoundException;
import com.example.easynotes.model.Note;
import com.example.easynotes.repository.NoteRepository;
import com.example.easynotes.util.ResponseUtil;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@RestController
@RequestMapping("/api")
public class NoteController {

    @Autowired
    NoteRepository noteRepository;

    @GetMapping("/notes")
    public List<Note> getAllNotes() {
        return noteRepository.findAll();
    }

    @PostMapping("/notes")
    public String createNote(@Valid @RequestBody Note note) {
        try {
            if (StringUtils.isEmpty(note.getTitle())) {
                return ResponseUtil.invalid();
            }
            noteRepository.save(note);
            return ResponseUtil.success(ResponseUtil.returnNote(note));
        } catch (Exception e) {
            return ResponseUtil.serverError();
        }
    }

    @GetMapping("/notes/{id}")
    public String getNoteById(@PathVariable(value = "id") Long noteId) {
        try {
            Optional<Note> note = noteRepository.findById(noteId);
            if (note.isPresent()) {
                return ResponseUtil.success(ResponseUtil.returnNote(note.get()));
            }
            return ResponseUtil.notfound();
        } catch (Exception e) {
            return ResponseUtil.serverError();
        }
    }

    @PutMapping("/notes/{id}")
    public Note updateNote(@PathVariable(value = "id") Long noteId,
            @Valid @RequestBody Note noteDetails) {

        Note note = noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));

        note.setTitle(noteDetails.getTitle());
        note.setContent(noteDetails.getContent());

        Note updatedNote = noteRepository.save(note);
        return updatedNote;
    }

    @DeleteMapping("/notes/{id}")
    public ResponseEntity<?> deleteNote(@PathVariable(value = "id") Long noteId) {
        Note note = noteRepository.findById(noteId)
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));

        noteRepository.delete(note);

        return ResponseEntity.ok().build();
    }
}
