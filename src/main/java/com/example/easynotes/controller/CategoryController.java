/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.easynotes.controller;

import com.example.easynotes.model.Category;
import com.example.easynotes.repository.CategoryRepository;
import static com.example.easynotes.util.ResponseUtil.returnListCategory;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Phong
 */
@RestController
@RequestMapping("/api")
public class CategoryController {

    @Autowired
    CategoryRepository categoryRepository;

    @GetMapping(value = "/category", produces = "application/json")
    public String getAll() throws JsonProcessingException {
        return returnListCategory(categoryRepository.findAll()).toString();
    }

    @GetMapping(value = "/category_by_name", produces = "application/json")
    public String getByName(HttpServletRequest httpServletRequest) throws JsonProcessingException {
        String name = httpServletRequest.getParameter("name");
        List<Category> categorys = categoryRepository.findByTitleJPQLQuery(name);
        String response = returnListCategory(categorys).toString();
        return response;
    }

    @PostMapping("/category")
    public Category create(@Valid @RequestBody Category category) {
        return categoryRepository.save(category);
    }
}
